package Ce;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Scanner;

public class FileCipher {
    private Scanner         input;
    private PrintStream     output;

    public FileCipher(String input, String output) {
        File file = new File(input);
        try {
            this.input  = new Scanner(file);
            this.output = new PrintStream(output);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public String getFirstWord(int length){
        String line = this.input.nextLine();
        return line.substring(0, length);
    }

    public void encode(int key){
        Caesar caesar = new Caesar(key);
        while(this.input.hasNextLine())
            this.output.print(
                    caesar.encode(this.input.nextLine()) + "\n"
            );
    }

    public void decode(int key){
        Caesar caesar = new Caesar(key);
        while(this.input.hasNextLine())
            this.output.print(
                    caesar.decode(this.input.nextLine()) + "\n"
            );
    }

    public void addLine(String text){
        this.output.print(text);
    }
}
