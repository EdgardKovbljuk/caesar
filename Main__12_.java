package Ce;

import java.io.File;
import java.io.PrintStream;
import java.util.Scanner;


class Main {
	public static void print(String s){System.out.println(s);}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
  		System.out.println("What file would you like to open?");
  		String input = scanner.nextLine();

		System.out.println("Where would you like to save en/decoded text?");
		String output = scanner.nextLine();

		FileCipher cipher = new FileCipher(input, output);
    	
		System.out.println("What would you like to do:" +
				"\n-encode" +
				"\n-decode" +
				"\n-get key");
		String operation = scanner.nextLine();

		switch(operation){
			case "encode": {
				print("Enter the key");
				cipher.encode(scanner.nextInt());
				print("File has been encoded !!!");
				print("You can open it there: " + output);
				break;
			}

			case "decode": {
				print("Enter the key");
				cipher.decode(scanner.nextInt());				
				print("File has been decoded !!!");
				print("You can open it there: " + output);
				break;
				
			}

			case "get key": {
				print("Enter decrypted version of the 1st word in the text");
				String wordDecrypted = scanner.nextLine();
				String wordEncrypted = cipher.getFirstWord(wordDecrypted.length());
				Caesar caesar = new Caesar();
				int key = caesar.getKeyFromWords(wordEncrypted,wordDecrypted);
				if (key == -100)
					print("Looks like you've entered bad word. Cannot find the key đź�˘");
				else
					print("Good. Got the key, it's " + key);
				break;
			}
		}    
	}
}


